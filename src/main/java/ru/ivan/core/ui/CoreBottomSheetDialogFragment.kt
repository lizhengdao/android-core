package ru.ivan.core.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.kodein.di.Kodein
import ru.ivan.core.BaseApp
import ru.ivan.core.ext.ContextAware
import ru.ivan.core.ext.colors
import ru.ivan.core.ext.setStatusBarColor
import ru.ivan.core.unclassifiedcommonmodels.navigation.ChildKodeinProvider
import ru.ivan.core.unclassifiedcommonmodels.navigation.ParentRouterProvider
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.CoordinatorRouter
import ru.ivan.core.util.retainedinstancemanager.IHasRetainedInstance
import ru.ivan.core.util.retainedinstancemanager.IdProvider
import java.util.*

abstract class CoreBottomSheetDialogFragment<Binding : ViewBinding> : BottomSheetDialogFragment(),
    ContextAware,
    IHasRetainedInstance<Kodein>, IdProvider {
    protected open val statusBarId: Int? = null
    protected var originalInputMode: Int? = null
    protected open val inputMode: Int? = null

    protected val recyclers: MutableList<RecyclerView> =
        mutableListOf()//Список ресайклеров. Нужен, чтоб занулялись адаптеры
    protected val pagers: MutableList<ViewPager2> =
        mutableListOf()//Список пейджеров. Нужен, чтоб занулялись адаптеры

    protected var _binding: Binding? = null
    protected val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        val args = arguments

        when {
            args == null -> {
                val bundle = Bundle()
                bundle.putString(
                    CONTAINER_UUID, UUID.randomUUID().toString()
                )
                arguments = bundle
            }
            args.getString(CONTAINER_UUID) == null -> args.putString(
                CONTAINER_UUID, UUID.randomUUID().toString()
            )
        }

        initDi()

        super.onCreate(savedInstanceState)
    }

    override fun onDestroyView() {
        recyclers.forEach { it.adapter = null }
        recyclers.clear()

        pagers.forEach { it.adapter = null }
        pagers.clear()

        if (originalInputMode != null && inputMode != null) activity?.window?.setSoftInputMode(
            originalInputMode!!
        )

        super.onDestroyView()

        _binding = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        originalInputMode = activity?.window?.attributes?.softInputMode
        inputMode?.let { activity?.window?.setSoftInputMode(it) }

        _binding = provideViewBinding(inflater, container)
        val view = binding.root

        prepareUi(savedInstanceState)

        return view
    }

    override fun onResume() {
        statusBarId?.let { setStatusBarColor(colors[it]) }
        super.onResume()
    }

    final override fun getUuid(): String = requireArguments().getString(
        CONTAINER_UUID
    )!!

    open fun initDi() = Unit

    open fun prepareUi(savedInstanceState: Bundle?) = Unit

    abstract fun provideViewBinding(inflater: LayoutInflater, container: ViewGroup?): Binding

    override fun getContext(): Context = super.getContext()!!

    protected fun getClosestParentKodein(): Kodein = when {
        parentFragment is ChildKodeinProvider -> (requireParentFragment() as ChildKodeinProvider).getChildKodein()
        activity is ChildKodeinProvider -> (requireActivity() as ChildKodeinProvider).getChildKodein()
        else -> BaseApp.appInstance.kodein
    }

    protected fun tryTyGetParentRouter(): CoordinatorRouter = when {
        parentFragment is ParentRouterProvider -> (parentFragment as ParentRouterProvider).getParentRouter()
        activity is ParentRouterProvider -> (activity as ParentRouterProvider).getParentRouter()
        else -> throw RuntimeException("Wtf")
    }

    companion object {
        private val CONTAINER_UUID =
            "${CoreBottomSheetDialogFragment::class.java.canonicalName}.CONTAINER_UUID"
    }
}