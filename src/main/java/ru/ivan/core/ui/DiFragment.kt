package ru.ivan.core.ui

import android.os.Bundle
import android.widget.Toast
import androidx.viewbinding.ViewBinding
import com.badoo.mvicore.ModelWatcher
import com.badoo.mvicore.android.AndroidTimeCapsule
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.ObservableSource
import io.reactivex.functions.Consumer
import org.kodein.di.Kodein
import org.kodein.di.LateInitKodein
import org.kodein.di.LazyKodein
import ru.ivan.core.di.KodeinInjectionManager

abstract class DiFragment<Binding : ViewBinding, ViewModel, UiEvent, News> :
    CoreFragment<Binding>() {
    protected val kodein = LateInitKodein()
    protected val parentKodein = LateInitKodein()

    protected lateinit var viewModelWatcher: ModelWatcher<ViewModel>
    protected open val viewModelConsumer = Consumer<ViewModel> { viewModelWatcher(it) }
    private val uiEvents: PublishRelay<UiEvent> = PublishRelay.create()
    protected val newsConsumer = Consumer<News> {
        processNews(it)
    }

    protected val uiEventsObservableSource = ObservableSource<UiEvent> { uiEvents.subscribe(it) }

    protected lateinit var timeCapsule: AndroidTimeCapsule

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        timeCapsule = AndroidTimeCapsule(savedInstanceState)
        provideBindings().setup(viewModelConsumer, uiEventsObservableSource, newsConsumer)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        timeCapsule.saveState(outState)
    }

    protected fun showErrorMsg(message: String) =
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()

    protected fun processBaseNews(news: BaseFeature.BaseNews) {
        when (news) {
            is BaseFeature.BaseNews.Message -> showErrorMsg(news.msg)
        }
    }

    protected fun sendUiEvent(event: UiEvent) = uiEvents.accept(event)

    override fun prepareUi(savedInstanceState: Bundle?) {
        super.prepareUi(savedInstanceState)

        viewModelWatcher = provideModelWatcher()
    }

    final override fun initDi() {
        parentKodein.baseKodein = getClosestParentKodein()
        kodein.baseKodein = KodeinInjectionManager.instance.bindKodein(this)
    }

    final override fun createRetainedInstance(): Kodein = LazyKodein {
        Kodein {
            extend(parentKodein)
            import(provideDiModule(), allowOverride = true)
        }
    }

    abstract fun provideModelWatcher(): ModelWatcher<ViewModel>

    abstract fun provideBindings(): BindingsBase<ViewModel, UiEvent, News>

    abstract fun provideDiModule(): Kodein.Module

    abstract fun processNews(news: News)
}