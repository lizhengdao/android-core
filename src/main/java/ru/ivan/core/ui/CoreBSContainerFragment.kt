package ru.ivan.core.ui

import android.app.Dialog
import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import ru.ivan.core.R
import ru.ivan.core.ext.inTransaction
import ru.ivan.core.unclassifiedcommonmodels.navigation.BackButtonListener
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.Coordinator
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.CoordinatorHolder
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router

abstract class CoreBSContainerFragment<Binding : ViewBinding> :
    CoreBottomSheetDialogFragment<Binding>() {
    //region Инъекции
    //endregion

    protected abstract val localCicerone: Cicerone<Router>

    protected abstract val coordinatorHolder: CoordinatorHolder?
    protected abstract val coordinator: Coordinator?
    protected abstract val navigator: Navigator

    //region Перегруженные методы
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            rootScreen()?.let {
                childFragmentManager.inTransaction {
                    replace(
                        R.id.fragmentContainer,
                        it
                    )
                }
            }
        }
        coordinatorHolder?.setCoordinator(coordinator)
    }

    override fun onResume() {
        super.onResume()
        localCicerone.navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        localCicerone.navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onDestroy() {
        coordinatorHolder?.removeCoordinator()
        super.onDestroy()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        //Перехват onBackPressed и перенаправление в роутер контейнера
        dialog.setOnKeyListener { _, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP) {
                if ((childFragmentManager.findFragmentById(
                        R.id.fragmentContainer
                    ) as? BackButtonListener)?.onBackPressed() != true
                ) {
                    localCicerone.router.exit()
                }
                return@setOnKeyListener true
            }
            return@setOnKeyListener false
        }

        return dialog
    }

    //endregion

    open fun rootScreen(): Fragment? = null
}