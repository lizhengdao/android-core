package ru.ivan.core.ui.adapters.base

import androidx.recyclerview.widget.DiffUtil

/**
 * Created by i.sokolovskiy on 22.10.19.
 */
class BaseDiffCallback : DiffUtil.ItemCallback<DiffItem>() {

    override fun areItemsTheSame(oldItem: DiffItem, newItem: DiffItem): Boolean =
        oldItem.areItemsTheSame(newItem)

    override fun areContentsTheSame(oldItem: DiffItem, newItem: DiffItem): Boolean =
        oldItem.areContentsTheSame(newItem)
}