package ru.ivan.core.ui

import android.os.Bundle
import android.widget.Toast
import androidx.viewbinding.ViewBinding
import com.badoo.mvicore.ModelWatcher
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.ObservableSource
import io.reactivex.functions.Consumer
import org.kodein.di.Kodein
import org.kodein.di.LateInitKodein
import org.kodein.di.LazyKodein
import org.kodein.di.generic.instance
import org.kodein.di.generic.on
import ru.ivan.core.BaseApp
import ru.ivan.core.di.KodeinInjectionManager
import ru.ivan.core.managers.ActivityResultManager
import ru.ivan.core.managers.PermissionsManager
import ru.ivan.core.unclassifiedcommonmodels.navigation.ChildKodeinProvider
import ru.ivan.core.unclassifiedcommonmodels.navigation.ParentRouterProvider
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.Coordinator
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.CoordinatorHolder
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.CoordinatorRouter
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router

abstract class DiActivity<Binding : ViewBinding, ViewModel, UiEvent, News> :
    CoreActivity<Binding>(), ChildKodeinProvider, ParentRouterProvider {
    protected val kodein = LateInitKodein()

    protected lateinit var viewModelWatcher: ModelWatcher<ViewModel>
    protected open val viewModelConsumer = Consumer<ViewModel> { viewModelWatcher(it) }
    private val uiEvents: PublishRelay<UiEvent> = PublishRelay.create()
    protected val newsConsumer = Consumer<News> {
        processNews(it)
    }

    protected val uiEventsObservableSource = ObservableSource<UiEvent> { uiEvents.subscribe(it) }

    protected val localCicerone: Cicerone<Router> by kodein.on(context = this).instance()
    protected val coordinatorHolder: CoordinatorHolder by kodein.on(context = this).instance()

    override val permissionManager: PermissionsManager by kodein.on(context = this).instance()
    override val activityResultManager: ActivityResultManager by kodein.on(context = this)
        .instance()

    lateinit var coordinator: Coordinator
    lateinit var navigator: Navigator
    //endregion


    //region Перегруженные методы
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        navigator = provideNavigator()
        coordinator = provideCoordinator()

        provideBindings().setup(viewModelConsumer, uiEventsObservableSource, newsConsumer)

        coordinatorHolder.setCoordinator(coordinator)
    }

    override fun onPause() {
        localCicerone.navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        localCicerone.navigatorHolder.setNavigator(navigator)
    }


    override fun onDestroy() {
        coordinatorHolder.removeCoordinator()
        super.onDestroy()
    }

    protected fun showErrorMsg(message: String) =
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

    protected fun processBaseNews(news: BaseFeature.BaseNews) {
        when (news) {
            is BaseFeature.BaseNews.Message -> showErrorMsg(news.msg)
        }
    }

    final override fun createRetainedInstance(): Kodein = LazyKodein {
        Kodein {
            extend(BaseApp.appInstance.kodein)
            import(provideDiModule(), allowOverride = true)
        }
    }

    protected fun sendUiEvent(event: UiEvent) = uiEvents.accept(event)

    override fun prepareUi() {
        super.prepareUi()

        viewModelWatcher = provideModelWatcher()
    }

    final override fun initDi() {
        kodein.baseKodein = KodeinInjectionManager.instance.bindKodein(this)
    }

    final override fun getChildKodein(): Kodein = kodein
    final override fun getParentRouter(): CoordinatorRouter = coordinatorHolder

    abstract fun provideNavigator(): Navigator

    abstract fun provideCoordinator(): Coordinator

    abstract fun provideModelWatcher(): ModelWatcher<ViewModel>

    abstract fun provideBindings(): BindingsBase<ViewModel, UiEvent, News>

    abstract fun provideDiModule(): Kodein.Module

    abstract fun processNews(news: News)
}