package ru.ivan.core.ui

import io.reactivex.ObservableSource
import io.reactivex.functions.Consumer

/**
 * Created by i.sokolovskiy on 24.06.19.
 */
abstract class BindingsBase<ViewModel, UiEvent, News> {
    abstract fun setup(
        viewModelConsumer: Consumer<ViewModel>, uiEventsObservableSource: ObservableSource<UiEvent>,
        newsConsumer: Consumer<News>
    )
}