package ru.ivan.core.unclassifiedcommonmodels.dataprocessing

import ru.ivan.core.server.base.RequestResult

/**
 * Created by i.sokolovskiy on 29.01.20.
 */
sealed class DataLoadingEffect2<T> {
    class Loading<T> : DataLoadingEffect2<T>()
    data class Loaded<T>(val payload: T) :
        DataLoadingEffect2<T>()

    data class LoadingError<T>(val throwable: RequestResult.Error) : DataLoadingEffect2<T>()

    class Refreshing<T> : DataLoadingEffect2<T>()
    data class Refreshed<T>(val payload: T) :
        DataLoadingEffect2<T>()

    data class RefreshingError<T>(val throwable: RequestResult.Error) :
        DataLoadingEffect2<T>()

    val isLoading: Boolean
        get() = this is Loading
    val isLoaded: Boolean
        get() = this is Loaded
    val isLoadingError: Boolean
        get() = this is LoadingError

    val isRefreshing: Boolean
        get() = this is Refreshing
    val isRefreshed: Boolean
        get() = this is Refreshed
    val isRefreshingError: Boolean
        get() = this is RefreshingError
}