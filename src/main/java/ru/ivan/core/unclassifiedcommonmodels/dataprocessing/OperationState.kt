package ru.ivan.core.unclassifiedcommonmodels.dataprocessing

/**
 * Created by i.sokolovskiy on 29.01.20.
 */
enum class OperationState {
    PROGRESS, NETWORK_ERROR,
}