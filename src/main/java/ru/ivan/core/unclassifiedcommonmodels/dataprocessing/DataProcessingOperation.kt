package ru.ivan.core.unclassifiedcommonmodels.dataprocessing

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by i.sokolovskiy on 29.01.20.
 */
sealed class DataProcessingOperation : Parcelable {
    @Parcelize
    object Idle : DataProcessingOperation(), Parcelable

    @Parcelize
    class ProcessingData(val operationState: OperationState) : DataProcessingOperation(), Parcelable

    val inProgress: Boolean
        get() = (this as? ProcessingData)?.operationState == OperationState.PROGRESS
    val inError: Boolean
        get() = (this as? ProcessingData)?.operationState == OperationState.NETWORK_ERROR
    val isIdle: Boolean
        get() = this is Idle
}