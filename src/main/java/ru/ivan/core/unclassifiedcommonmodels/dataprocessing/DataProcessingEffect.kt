package ru.ivan.core.unclassifiedcommonmodels.dataprocessing

import ru.ivan.core.server.base.RequestResult

/**
 * Created by i.sokolovskiy on 29.01.20.
 */
sealed class DataProcessingEffect<T> {
    class Processing<T> : DataProcessingEffect<T>()
    data class Processed<T>(val payload: T) : DataProcessingEffect<T>()
    data class ProcessingError<T>(val throwable: RequestResult.Error) : DataProcessingEffect<T>()

    inline fun <V> processResult(
        processingHandler: (Processing<T>) -> V,
        successHandler: (Processed<T>) -> V,
        errorHandler: (ProcessingError<T>) -> V
    ): V =
        when (this) {
            is Processing -> processingHandler(this)
            is Processed -> successHandler(this)
            is ProcessingError -> errorHandler(this)
        }

    val isProcessing: Boolean
        get() = this is Processing
    val isProcessed: Boolean
        get() = this is Processed
    val isError: Boolean
        get() = this is ProcessingError
}