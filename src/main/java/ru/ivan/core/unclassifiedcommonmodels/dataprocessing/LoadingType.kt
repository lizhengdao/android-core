package ru.ivan.core.unclassifiedcommonmodels.dataprocessing

/**
 * Created by i.sokolovskiy on 31.01.20.
 */
enum class LoadingType { LOAD, REFRESH }