package ru.ivan.core.unclassifiedcommonmodels

import android.os.Bundle
import androidx.fragment.app.Fragment
import ru.ivan.core.R
import ru.ivan.core.ext.inTransaction
import ru.ivan.core.unclassifiedcommonmodels.navigation.BackButtonListener
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.Coordinator
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.CoordinatorHolder
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router

/**
 * @deprecated use [ru.ivan.core.ui.CoreContainerFragment]
 */
@Deprecated("")
abstract class CoreContainerFragment : CoreFragment(), BackButtonListener {
    override val layoutRes = R.layout.view_fragment_conatiner

    //region Инъекции
    //endregion

    protected abstract val localCicerone: Cicerone<Router>

    protected abstract val coordinatorHolder: CoordinatorHolder?
    protected abstract val coordinator: Coordinator?
    protected abstract val navigator: Navigator

    //region Перегруженные методы
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            rootScreen()?.let {
                childFragmentManager.inTransaction {
                    replace(
                        R.id.fragmentContainer,
                        it
                    )
                }
            }
        }
        coordinatorHolder?.setCoordinator(coordinator)
    }

    override fun onResume() {
        super.onResume()
        localCicerone.navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        localCicerone.navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onDestroy() {
        coordinatorHolder?.removeCoordinator()
        super.onDestroy()
    }

    override fun onBackPressed(): Boolean {
        if ((childFragmentManager.findFragmentById(
                R.id.fragmentContainer
            ) as? BackButtonListener)?.onBackPressed() == true
        ) {
            return true
        }

        localCicerone.router.exit()
        return true
    }
    //endregion

    open fun rootScreen(): Fragment? = null
}