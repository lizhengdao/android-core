package ru.ivan.core.unclassifiedcommonmodels

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.kodein.di.Kodein
import ru.ivan.core.BaseApp
import ru.ivan.core.ext.ContextAware
import ru.ivan.core.unclassifiedcommonmodels.navigation.ChildKodeinProvider
import ru.ivan.core.unclassifiedcommonmodels.navigation.ParentRouterProvider
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.CoordinatorRouter
import ru.ivan.core.util.retainedinstancemanager.IHasRetainedInstance
import ru.ivan.core.util.retainedinstancemanager.IdProvider
import java.util.*

/**
 * @deprecated use [ru.ivan.core.ui.DiBottomSheetDialogFragment]
 */
@Deprecated("")
abstract class BaseBottomSheetDialogFragment : BottomSheetDialogFragment(), ContextAware,
    IHasRetainedInstance<Kodein>, IdProvider {
    abstract val layoutRes: Int

    protected val recyclers: MutableList<RecyclerView> =
        mutableListOf()//Список ресайклеров. Нужен, чтоб занулялись адаптеры
    protected val pagers: MutableList<ViewPager2> =
        mutableListOf()//Список пейджеров. Нужен, чтоб занулялись адаптеры


    override fun onCreate(savedInstanceState: Bundle?) {
        val args = arguments

        when {
            args == null -> {
                val bundle = Bundle()
                bundle.putString(CONTAINER_UUID, UUID.randomUUID().toString())
                arguments = bundle
            }
            args.getString(CONTAINER_UUID) == null -> args.putString(
                CONTAINER_UUID, UUID.randomUUID().toString()
            )
        }

        initDi()

        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(layoutRes, container, false)

        prepareUi(view, savedInstanceState)
        return view
    }

    override fun onStart() {
        super.onStart()
        val window = dialog?.window

        // нижний бар отображается корректно (не затемняется)
        if (window != null && Build.VERSION.SDK_INT >= 21) {
            window.clearFlags(
                WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS or
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
            )
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
        }
    }

    override fun onDestroyView() {
        recyclers.forEach { it.adapter = null }
        recyclers.clear()

        pagers.forEach { it.adapter = null }
        pagers.clear()

        super.onDestroyView()
    }

    override fun getUuid(): String = requireArguments().getString(CONTAINER_UUID)!!

    open fun prepareUi(view: View, savedInstanceState: Bundle?) = Unit

    open fun initDi() = Unit

    override fun getContext(): Context = super.getContext()!!

    protected fun showErrorMsg(message: String) =
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()

    protected fun getClosestParentKodein(): Kodein = when {
        parentFragment is ChildKodeinProvider -> (requireParentFragment() as ChildKodeinProvider).getChildKodein()
        activity is ChildKodeinProvider -> (requireActivity() as ChildKodeinProvider).getChildKodein()
        else -> BaseApp.appInstance.kodein
    }

    protected fun tryTyGetParentRouter(): CoordinatorRouter = when {
        parentFragment is ParentRouterProvider -> (parentFragment as ParentRouterProvider).getParentRouter()
        activity is ParentRouterProvider -> (activity as ParentRouterProvider).getParentRouter()
        else -> throw RuntimeException("Wtf")
    }

    companion object {
        private val CONTAINER_UUID =
            "${BaseBottomSheetDialogFragment::class.java.canonicalName}.CONTAINER_UUID"
    }
}