package ru.ivan.core.unclassifiedcommonmodels.navigation

interface BackButtonListener {
    fun onBackPressed(): Boolean
}