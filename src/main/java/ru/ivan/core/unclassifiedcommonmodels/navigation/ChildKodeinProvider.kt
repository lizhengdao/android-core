package ru.ivan.core.unclassifiedcommonmodels.navigation

import org.kodein.di.Kodein

interface ChildKodeinProvider {
    fun getChildKodein(): Kodein
}