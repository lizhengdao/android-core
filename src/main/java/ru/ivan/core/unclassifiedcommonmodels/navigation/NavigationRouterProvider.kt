package ru.ivan.core.unclassifiedcommonmodels.navigation

import ru.terrakok.cicerone.Router

interface NavigationRouterProvider {
    fun getNavigationRouter(): Router
}