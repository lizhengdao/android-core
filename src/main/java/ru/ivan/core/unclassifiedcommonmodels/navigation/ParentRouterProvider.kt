package ru.ivan.core.unclassifiedcommonmodels.navigation

import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.CoordinatorRouter

/**
 * Created by i.sokolovskiy on 17.12.19.
 */
interface ParentRouterProvider {
    fun getParentRouter(): CoordinatorRouter
}