package ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator

interface Coordinator {
    fun consumeEvent(event: CoordinatorEvent)
}