package ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator

class StubCoordinator : Coordinator {
    override fun consumeEvent(event: CoordinatorEvent) = Unit
}