package ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator

interface CoordinatorRouter {
    fun sendEvent(event: CoordinatorEvent)
}