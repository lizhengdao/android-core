package ru.ivan.core.unclassifiedcommonmodels

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import org.kodein.di.Kodein
import ru.ivan.core.BaseApp
import ru.ivan.core.ext.ContextAware
import ru.ivan.core.ext.colors
import ru.ivan.core.ext.setStatusBarColor
import ru.ivan.core.unclassifiedcommonmodels.navigation.ChildKodeinProvider
import ru.ivan.core.unclassifiedcommonmodels.navigation.ParentRouterProvider
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.CoordinatorRouter
import ru.ivan.core.util.retainedinstancemanager.IHasRetainedInstance
import ru.ivan.core.util.retainedinstancemanager.IdProvider
import java.util.*

/**
 * @deprecated use [ru.ivan.core.ui.CoreFragment]
 */
@Deprecated("")
abstract class CoreFragment : Fragment(), ContextAware, IHasRetainedInstance<Kodein>, IdProvider {
    abstract val layoutRes: Int
    protected open val statusBarId: Int? = null
    protected var originalInputMode: Int? = null
    protected open val inputMode: Int? = null

    protected val recyclers: MutableList<RecyclerView> =
        mutableListOf()//Список ресайклеров. Нужен, чтоб занулялись адаптеры
    protected val pagers: MutableList<ViewPager2> =
        mutableListOf()//Список пейджеров. Нужен, чтоб занулялись адаптеры

    override fun onCreate(savedInstanceState: Bundle?) {
        val args = arguments

        when {
            args == null -> {
                val bundle = Bundle()
                bundle.putString(CONTAINER_UUID, UUID.randomUUID().toString())
                arguments = bundle
            }
            args.getString(CONTAINER_UUID) == null -> args.putString(
                CONTAINER_UUID, UUID.randomUUID().toString()
            )
        }

        initDi()

        super.onCreate(savedInstanceState)
    }

    override fun onDestroyView() {
        recyclers.forEach { it.adapter = null }
        recyclers.clear()

        pagers.forEach { it.adapter = null }
        pagers.clear()

        if (originalInputMode != null && inputMode != null) activity?.window?.setSoftInputMode(
            originalInputMode!!
        )

        super.onDestroyView()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        originalInputMode = activity?.window?.attributes?.softInputMode
        inputMode?.let { activity?.window?.setSoftInputMode(it) }

        val view = inflater.inflate(layoutRes, container, false)

        prepareUi(view, savedInstanceState)
        return view
    }

    override fun onResume() {
        statusBarId?.let { setStatusBarColor(colors[it]) }
        super.onResume()
    }

    override fun getUuid(): String = requireArguments().getString(CONTAINER_UUID)!!

    open fun initDi() = Unit

    open fun prepareUi(view: View, savedInstanceState: Bundle?) = Unit

    override fun getContext(): Context = super.getContext()!!

    protected fun getClosestParentKodein(): Kodein = when {
        parentFragment is ChildKodeinProvider -> (requireParentFragment() as ChildKodeinProvider).getChildKodein()
        activity is ChildKodeinProvider -> (requireActivity() as ChildKodeinProvider).getChildKodein()
        else -> BaseApp.appInstance.kodein
    }

    protected fun tryTyGetParentRouter(): CoordinatorRouter = when {
        parentFragment is ParentRouterProvider -> (parentFragment as ParentRouterProvider).getParentRouter()
        activity is ParentRouterProvider -> (activity as ParentRouterProvider).getParentRouter()
        else -> throw RuntimeException("Wtf")
    }

    companion object {
        private val CONTAINER_UUID = "${CoreFragment::class.java.canonicalName}.CONTAINER_UUID"
    }
}