package ru.ivan.core.unclassifiedcommonmodels

import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import org.kodein.di.KodeinAware
import ru.ivan.core.BaseApp
import ru.ivan.core.BaseScreens
import ru.ivan.core.ext.browse
import ru.ivan.core.ext.closeKeyboard
import ru.ivan.core.ext.email
import ru.ivan.core.ext.openPhone
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Forward
import java.lang.ref.WeakReference

/**
 * Created by i.sokolovskiy on 04.02.19.
 */
open class CustomNavigator : SupportAppNavigator, KodeinAware {
    override val kodein by lazy { BaseApp.appInstance.kodein }

    val onExit: (() -> Unit)?

    private val activityWeak: WeakReference<FragmentActivity>

    constructor(activity: FragmentActivity, containerId: Int, onExit: (() -> Unit)?) : super(
        activity,
        containerId
    ) {
        this.activityWeak = WeakReference(activity)
        this.onExit = onExit
    }

    constructor(
        activity: FragmentActivity,
        fragmentManager: FragmentManager,
        containerId: Int,
        onExit: (() -> Unit)?
    ) : super(activity, fragmentManager, containerId) {
        this.activityWeak = WeakReference(activity)
        this.onExit = onExit
    }

    override fun applyCommands(commands: Array<out Command>) {
        val activity = activityWeak.get()
        if (activity == null) return
        activity.runOnUiThread {
            activity.closeKeyboard()
            super.applyCommands(commands)
        }
    }

    override fun applyCommand(command: Command) {
        when (command) {
            is Forward -> {
                val screen = command.screen
                when (screen) {
                    is BaseScreens.WebBrowser -> {
                        val activity = activityWeak.get()
                        if (activity == null) return

                        activity.browse((command.screen as BaseScreens.WebBrowser).site)
                        return
                    }
                    is BaseScreens.Email -> {
                        val activity = activityWeak.get()
                        if (activity == null) return

                        activity.email((command.screen as BaseScreens.Email).mail)
                        return
                    }
                    is BaseScreens.Phone -> {
                        activityWeak.get()?.apply {
                            openPhone((command.screen as BaseScreens.Phone).phone)
                        }
                        return
                    }
                }
            }
        }

        super.applyCommand(command)
    }

    override fun activityBack() = onExit?.invoke() ?: super.activityBack()
}