package ru.ivan.core.server.base

/**
 * Created by i.sokolovskiy on 25.12.19.
 */
interface IServerError {
    fun getCode(): String
    fun getDetail(): String
}