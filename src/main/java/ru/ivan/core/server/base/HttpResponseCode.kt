package ru.ivan.core.server.base

/**
 * Created by i.sokolovskiy on 25.06.19.
 */
enum class HttpResponseCode {
    CODE_400, CODE_403, CODE_404, CODE_409
}