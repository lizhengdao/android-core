package ru.ivan.core.server.base

/**
 * Created by i.sokolovskiy on 25.06.19.
 */
typealias HttpCodeHandler = Pair<HttpResponseCode, ((errorList: List<IServerError>) -> Boolean)>