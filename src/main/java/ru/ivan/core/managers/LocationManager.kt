package ru.ivan.core.managers

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.location.Location
import android.util.Log
import androidx.annotation.RequiresPermission
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.disposables.Disposables
import ru.ivan.core.unclassifiedcommonmodels.OperationResult

/**
 * Created by i.sokolovskiy on 15.08.19.
 */
class LocationManager(
    private val context: Context,
    private val permissionsManager: PermissionsManager,
    private val activityResultManager: ActivityResultManager
) {
    private val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

    sealed class LocResult {
        object CoordinatesNotAvailable : LocResult()

        data class Success(val location: Location) : LocResult()

        object LastLocationExceptionNotResolved : LocResult()
        object CheckLocationSettingsExceptionNotResolved : LocResult()
        data class UnresolvableException(val ex: Exception) : LocResult()
    }

    fun getLocation(): Observable<Result> =
        permissionsManager
            .check(PermissionsManager.LOCATION)
            .flatMap { list ->
                if (list.isEmpty())
                    return@flatMap checkLocationSettingsAndGetLocationRecursive(true)
                        .map { Result.Success(it) }

                return@flatMap permissionsManager.request(list.toTypedArray())
                    .flatMap { result ->
                        if (result.items.isEmpty())
                            return@flatMap checkLocationSettingsAndGetLocationRecursive(true)
                                .map { Result.Success(it) }

                        val showDialogList = result.items.filter {
                            it.shouldShowRequestPermissionRationale
                        }

                        Observable.just(
                            if (showDialogList.isNotEmpty()) Result.ShouldShowRationaleDialog(
                                showDialogList
                            ) else Result.ShouldGoToSettings
                        )
                    }
            }

    sealed class Result {
        data class Success(val result: LocResult) : Result()
        object ShouldGoToSettings : Result()
        data class ShouldShowRationaleDialog(val list: List<PermissionsManager.Permission>) :
            Result()
    }

    @SuppressLint("MissingPermission")
    private fun checkLocationSettingsAndGetLocationRecursive(needToResolveException: Boolean): Observable<LocResult> =
        checkLocationSettings()
            .flatMap {
                when (it) {
                    is OperationResult.Ok -> {
                        Log.d(
                            LocationManager::class.java.simpleName,
                            "checkLocationSettingsAndGetLocationRecursive: разрешения на геолокацию проверены"
                        )
                        Log.d(
                            LocationManager::class.java.simpleName,
                            "checkLocationSettingsAndGetLocationRecursive: попытка получить поледнюю геопозицию"
                        )
                        tryToGetLastLocationRecursive(true)
                    }

                    is OperationResult.Fail -> {
                        Log.d(
                            LocationManager::class.java.simpleName,
                            "checkLocationSettingsAndGetLocationRecursive: не удалось проверить разрешение на геолокации"
                        )

                        if (it.payload !is ResolvableApiException) {
                            Log.d(
                                LocationManager::class.java.simpleName,
                                "checkLocationSettingsAndGetLocationRecursive: произошло неразрешимое исключение"
                            )
                            return@flatMap Observable.just(LocResult.UnresolvableException(it.payload))
                        }

                        if (needToResolveException) {
                            Log.d(
                                LocationManager::class.java.simpleName,
                                "checkLocationSettingsAndGetLocationRecursive: попытка разрешить exception, возникший при проверке разрешения на геолокации и рекурсивный вызов checkLocationSettingsRecursive в случае успеха"
                            )

                            tryToResolveCheckLocationSettingsException(it.payload)
                                .flatMap {
                                    when (it) {
                                        is OperationResult.Ok -> {
                                            Log.d(
                                                LocationManager::class.java.simpleName,
                                                "checkLocationSettingsAndGetLocationRecursive: exception, возникший при проверке разрешения на геолокации разрешен"
                                            )
                                            Log.d(
                                                LocationManager::class.java.simpleName,
                                                "checkLocationSettingsAndGetLocationRecursive: рекурсивный вызов checkLocationSettingsRecursive"
                                            )
                                            checkLocationSettingsAndGetLocationRecursive(false)
                                        }
                                        is OperationResult.Fail -> {
                                            Log.d(
                                                LocationManager::class.java.simpleName,
                                                "checkLocationSettingsAndGetLocationRecursive: не удалось разрешить exception возникший при проверке разрешения на геолокации"
                                            )
                                            Observable.just(LocResult.CheckLocationSettingsExceptionNotResolved)
                                        }
                                    }
                                }
                        } else {
                            Log.d(
                                LocationManager::class.java.simpleName,
                                "checkLocationSettingsAndGetLocationRecursive: не удалось разрешить exception возникший при проверке разрешения на геолокации"
                            )
                            Observable.just(LocResult.CheckLocationSettingsExceptionNotResolved)
                        }
                    }
                }
            }

    @SuppressLint("MissingPermission")
    private fun tryToGetLastLocationRecursive(needToResolveException: Boolean): Observable<LocResult> =
        tryToGetLastLocation()
            .flatMap {
                when (it) {
                    is OperationResult.Ok -> {
                        if (it.payload == null) {
                            Log.d(
                                LocationManager::class.java.simpleName,
                                "tryToGetLastLocationRecursive: последняя геопозиция получена, но она равна null"
                            )
                            Log.d(
                                LocationManager::class.java.simpleName,
                                "tryToGetLastLocationRecursive: подписываемся на получении последней геопозиции, когда она появится"
                            )
                            getLastLocationUpdates()
                                .flatMap {
                                    when (it) {
                                        is OperationResult.Ok -> {
                                            if (it.payload == null) {
                                                Log.d(
                                                    LocationManager::class.java.simpleName,
                                                    "tryToGetLastLocationRecursive: последняя геопозиция получена, но она равна null"
                                                )
                                                Observable.just(LocResult.CoordinatesNotAvailable)
                                            } else {
                                                Log.d(
                                                    LocationManager::class.java.simpleName,
                                                    "tryToGetLastLocationRecursive: последняя геопозиция получена"
                                                )
                                                Observable.just(LocResult.Success(it.payload))
                                            }
                                        }
                                        is OperationResult.Fail -> {
                                            Log.d(
                                                LocationManager::class.java.simpleName,
                                                "tryToGetLastLocationRecursive: не удалось получить последнюю геопозицию"
                                            )
                                            Observable.just(LocResult.CoordinatesNotAvailable)
                                        }
                                    }
                                }
                        } else {
                            Log.d(
                                LocationManager::class.java.simpleName,
                                "tryToGetLastLocationRecursive: последняя геопозиция получена"
                            )
                            Observable.just(LocResult.Success(it.payload))
                        }
                    }
                    is OperationResult.Fail -> {
                        Log.d(
                            LocationManager::class.java.simpleName,
                            "tryToGetLastLocationRecursive: не удалось получить поледнюю геопозицию"
                        )

                        if (it.payload !is ResolvableApiException) {
                            Log.d(
                                LocationManager::class.java.simpleName,
                                "tryToGetLastLocationRecursive: произошло неразрешимое исключение"
                            )
                            return@flatMap Observable.just(LocResult.UnresolvableException(it.payload))
                        }

                        if (needToResolveException) {
                            Log.d(
                                LocationManager::class.java.simpleName,
                                "tryToGetLastLocationRecursive: попытка разрешить exception, возникший при получении последней геопозиции и рекурсивный вызов tryToGetLastLocationRecursive в случае успеха"
                            )

                            return@flatMap tryToResolveLastLocationGettingException(it.payload)
                                .flatMap {
                                    when (it) {
                                        is OperationResult.Ok -> {
                                            Log.d(
                                                LocationManager::class.java.simpleName,
                                                "tryToGetLastLocationRecursive: exception, возникший при получении последней геопозиции разрешен"
                                            )
                                            Log.d(
                                                LocationManager::class.java.simpleName,
                                                "tryToGetLastLocationRecursive: рекурсивный вызов tryToGetLastLocationRecursive"
                                            )
                                            tryToGetLastLocationRecursive(false)
                                        }
                                        is OperationResult.Fail -> {
                                            Log.d(
                                                LocationManager::class.java.simpleName,
                                                "tryToGetLastLocationRecursive: не удалось разрешить exception при получении последней геопозиции"
                                            )
                                            Observable.just(LocResult.LastLocationExceptionNotResolved)
                                        }
                                    }
                                }
                        } else {
                            Log.d(
                                LocationManager::class.java.simpleName,
                                "tryToGetLastLocationRecursive: не удалось разрешить exception при получении последней геопозиции"
                            )
                            Observable.just(LocResult.LastLocationExceptionNotResolved)
                        }
                    }
                }
            }

    //Проверка настроек геолокации
    private fun checkLocationSettings(): Observable<OperationResult<Unit, Exception>> =
        Observable.create { emitter ->
            val locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(5000)

            val locationSettingsRequest = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest).build()

            LocationServices.getSettingsClient(context)
                .checkLocationSettings(locationSettingsRequest)
                .addOnSuccessListener { response ->
                    emitter.onNext(OperationResult.Ok(Unit))
                    emitter.onComplete()
                }
                .addOnFailureListener { exception ->
                    emitter.onNext(OperationResult.Fail(exception))
                    emitter.onComplete()
                }
        }

    //Попытка разрешить исключение, возникшее при проверке настроек геолокации
    private fun tryToResolveCheckLocationSettingsException(ex: ResolvableApiException): Observable<OperationResult<Unit, Unit>> =
        activityResultManager.resolveException(ex)
            .map {
                return@map if (it.resultCode == Activity.RESULT_OK) {
                    OperationResult.Ok<Unit, Unit>(Unit)
                } else {
                    OperationResult.Fail<Unit, Unit>(Unit)
                }
            }

    //Попытка достать последнюю геопозицию
    @RequiresPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    @SuppressWarnings("MissingPermission")
    @SuppressLint("MissingPermission")
    private fun tryToGetLastLocation(): Observable<OperationResult<Location?, Exception>> =
        Observable.create { emitter ->
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    emitter.onNext(OperationResult.Ok(location))
                    emitter.onComplete()
                }
                .addOnFailureListener { exception ->
                    emitter.onNext(OperationResult.Fail(exception))
                    emitter.onComplete()
                }
        }

    //Попытка разрешить исключение, возникшее при попытке достать последнюю геопозицию
    private fun tryToResolveLastLocationGettingException(ex: ResolvableApiException): Observable<OperationResult<Unit, Unit>> =
        activityResultManager.resolveException(ex)
            .map {
                return@map if (it.resultCode == Activity.RESULT_OK) {
                    OperationResult.Ok<Unit, Unit>(Unit)
                } else {
                    OperationResult.Fail<Unit, Unit>(Unit)
                }
            }

    //Получение подписки на последнюю геопозицию
    private fun getLastLocationUpdates(): Observable<OperationResult<Location?, Unit>> =
        Observable.create(UpdateLocationObservableSubscribe(fusedLocationClient))

    private class UpdateLocationObservableSubscribe(private val locationProvider: FusedLocationProviderClient) :
        ObservableOnSubscribe<OperationResult<Location?, Unit>> {
        private val locationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(5000)
            .setNumUpdates(1)

        @RequiresPermission(Manifest.permission.ACCESS_FINE_LOCATION)
        @SuppressWarnings("MissingPermission")
        @SuppressLint("MissingPermission")
        override fun subscribe(emitter: ObservableEmitter<OperationResult<Location?, Unit>>) {
            val locationCallback = object : LocationCallback() {
                override fun onLocationResult(result: LocationResult) {
                    locationProvider.removeLocationUpdates(this)

                    val lastLocation = result.lastLocation
                    emitter.onNext(OperationResult.Ok(lastLocation))

                    emitter.onComplete()
                }
            }

            locationProvider.requestLocationUpdates(locationRequest, locationCallback, null)
            emitter.setDisposable(Disposables.fromRunnable {
                locationProvider.removeLocationUpdates(
                    locationCallback
                )
            })
        }
    }
}