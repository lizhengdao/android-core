package ru.ivan.core.managers

import android.app.Activity
import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import androidx.core.content.FileProvider
import io.reactivex.Observable

class CameraManager(
    private val context: Context,
    private val permissionsManager: PermissionsManager,
    private val activityResultManager: ActivityResultManager,
    private val appFileManager: AppFileManager
) {
    private var filePath: String? = null
        get() {
            val tmp = field
            field = null
            return tmp
        }

    /** запуск камеры */
    fun startCamera(fileProviderName: String, requestCode: Int): Observable<CameraStartingResult> =
        permissionsManager
            .check(PermissionsManager.CAMERA, PermissionsManager.WRITE_EXTERNAL_STORAGE)
            .flatMap { list ->
                if (list.isEmpty()) {
                    return@flatMap Observable.fromCallable {
                        CameraStartingResult.CameraStarted(
                            capturePhoto(
                                fileProviderName,
                                requestCode
                            )
                        )
                    }
                }

                return@flatMap permissionsManager.request(list.toTypedArray())
                    .flatMap { result ->
                        if (result.items.isEmpty()) {
                            return@flatMap Observable.fromCallable {
                                CameraStartingResult.CameraStarted(
                                    capturePhoto(
                                        fileProviderName,
                                        requestCode
                                    )
                                )
                            }
                        }

                        val showDialogList = result.items.filter {
                            it.shouldShowRequestPermissionRationale
                        }
                        if (showDialogList.isNotEmpty()) {
                            Observable.just(
                                CameraStartingResult.CanNotShowPermissionDialog(showDialogList)
                            )
                        } else Observable.just(CameraStartingResult.NotEnoughPermissions)
                    }
            }

    fun getOnPhotoCapturedObservable(requestCode: Int): Observable<PhotoCapturingResult> =
        activityResultManager
            .getOnActivityResultObservable(requestCode)
            .map {
                when (it.resultCode) {
                    Activity.RESULT_OK -> PhotoCapturingResult.Success
                    else -> PhotoCapturingResult.Canceled
                }
            }

    private fun capturePhoto(fileProviderName: String, requestCode: Int): String {
        val file = appFileManager.createCachePhotoImage()!!

        filePath = file.absolutePath
        val photoURI: Uri = FileProvider.getUriForFile(context, fileProviderName, file)

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.apply {
            // для совместимости со страрыми версиями android
            clipData = ClipData.newUri(context.contentResolver, CACHE_IMAGE_URI, photoURI)
            addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION)
            putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
        }

        activityResultManager.startActivityForResult(intent, null, requestCode)
        return file.absolutePath
    }

    companion object {
        private const val CACHE_IMAGE_URI = "cache_image_uri"
    }

    sealed class CameraStartingResult {
        data class CameraStarted(val filePath: String) : CameraStartingResult()
        object NotEnoughPermissions : CameraStartingResult()
        data class CanNotShowPermissionDialog(val list: List<PermissionsManager.Permission>) :
            CameraStartingResult()
    }

    sealed class PhotoCapturingResult {
        object Success : PhotoCapturingResult()
        object Canceled : PhotoCapturingResult()
    }
}