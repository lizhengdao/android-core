package ru.ivan.core.managers

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import ru.ivan.core.util.Utils

/**
 * Created by i.sokolovskiy on 15.08.19.
 */
class PermissionsManager(private val context: Context) {

    // subject для запроса разрешений, subscribe в activity или fragment
    private val requestSubject: PublishSubject<Pair<Int, Array<String>>> =
        PublishSubject.create()
    val requestObservable: Observable<Pair<Int, Array<String>>>
        get() = requestSubject

    // subject получающий отсутствующие разрешения
    private val onRequestResultSubject: PublishSubject<Result> =
        PublishSubject.create()

    /** возвращает отсутствующие разрешения */
    fun check(vararg permissions: String): Observable<List<Permission>> =
        Observable.fromCallable {
            Log.e("PermissionsManager", "checkPermissions. Permissions checking")
            permissions.mapNotNull {
                when (ContextCompat.checkSelfPermission(context, it)) {
                    PackageManager.PERMISSION_DENIED ->
                        Permission(
                            it,
                            shouldShowRequestPermissionRationale = false
                        )

                    else -> null
                }
            }
        }

    /**
     * запрос разрешений
     * возвращает код и отсутствующие разрешения
     */
    fun request(permissions: Array<Permission>): Observable<Result> {
        val requestCode = Utils.generateRequestCode()
        return Observable
            .fromCallable {
                Log.e("PermissionsManager", "requestPermissions. Permissions requesting")
                requestSubject.onNext(
                    Pair(
                        requestCode,
                        permissions.map { it.permission }.toTypedArray()
                    )
                )
            }
            .flatMap { onRequestResultSubject }
            .filter { it.requestCode == requestCode }
    }

    /**
     * метод для получения результатов запроса
     * вызывается activity или fragment в методе onRequestPermissionsResult
     * передаёт в subject отсутствующие разрешения
     */
    fun onRequestResult(
        requestCode: Int,
        requestResultList: List<Permission>
    ) {
        onRequestResultSubject.onNext(Result(requestCode, requestResultList))
    }

    /**
     * метод для получения остутствующих разрешений
     * вызывается activity или fragment в методе onRequestPermissionsResult
     */


    companion object {
        const val CAMERA = Manifest.permission.CAMERA
        const val WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE
        const val LOCATION = Manifest.permission.ACCESS_FINE_LOCATION
        const val READ_CALENDAR = Manifest.permission.READ_CALENDAR
        const val WRITE_CALENDAR = Manifest.permission.WRITE_CALENDAR

        fun getPermissionList(
            activity: Activity,
            permissions: Array<out String>,
            grantResults: IntArray
        ): List<Permission> = permissions.mapIndexedNotNull { index, s ->
            if (grantResults[index] == PackageManager.PERMISSION_DENIED)
                Permission(s, ActivityCompat.shouldShowRequestPermissionRationale(activity, s))
            else null
        }
    }

    data class Permission(
        val permission: String,
        val shouldShowRequestPermissionRationale: Boolean
    )

    data class Result(
        val requestCode: Int,
        val items: List<Permission>
    )
}