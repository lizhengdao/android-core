package ru.ivan.core.managers

import android.content.Intent
import android.os.Bundle
import com.google.android.gms.common.api.ResolvableApiException
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import ru.ivan.core.util.Utils

/**
 * Created by i.sokolovskiy on 15.08.19.
 */
class ActivityResultManager {
    // subject для запуска activity, subscribe в activity или fragment
    private val onActivityResultSubject: PublishSubject<ActivityResult> = PublishSubject.create()
    val startActivityForResultSubject: PublishSubject<StartActivityRequest> =
        PublishSubject.create()

    val resolveExceptionSubject: PublishSubject<ResolveExceptionRequest> = PublishSubject.create()

    /**
     * Попытка разрешить exception
     */
    fun resolveException(ex: ResolvableApiException): Observable<ActivityResult> {
        val requestCode = Utils.generateRequestCode()
        return Observable
            .fromCallable {
                resolveExceptionSubject.onNext(ResolveExceptionRequest(ex, requestCode))
            }
            .flatMap { onActivityResultSubject }
            .filter { it.requestCode == requestCode }
            .take(1)
    }

    /**
     * запуск activity для получения результата
     * возвращает результат activity
     */
    fun startActivityForResult(intent: Intent, options: Bundle?, requestCode: Int) {
        startActivityForResultSubject.onNext(
            StartActivityRequest(
                intent,
                requestCode,
                options
            )
        )
    }

    fun getOnActivityResultObservable(requestCode: Int): Observable<ActivityResult> =
        onActivityResultSubject.filter { it.requestCode == requestCode }

    /**
     * метод для получения результатов запроса
     * вызывается activity или fragment в методе onActivityResult
     * результат передаёт в subject
     */
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        onActivityResultSubject.onNext(ActivityResult(requestCode, resultCode, data))
    }

    data class StartActivityRequest(
        val intent: Intent,
        val requestCode: Int,
        val options: Bundle?
    )

    data class ResolveExceptionRequest(
        val exception: ResolvableApiException,
        val requestCode: Int
    )
}

data class ActivityResult(
    val requestCode: Int,
    val resultCode: Int,
    val data: Intent?
)