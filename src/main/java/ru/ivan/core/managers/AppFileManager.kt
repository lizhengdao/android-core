package ru.ivan.core.managers

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import java.io.*

class AppFileManager(context: Context) {

    private val cacheImagePath =
        context.cacheDir.absolutePath + File.separator + IMAGE_DIR_NAME + File.separator
    private val cacheDir = File(checkPath(cacheImagePath))

    // создание изображения
    private fun checkPath(path: String): String {
        val dir = File(path)
        if (!dir.exists()) dir.mkdir()
        return path
    }

    fun createCachePhotoImage(): File? = try {
        File.createTempFile(filePrefix, fileSuffix, cacheDir)
    } catch (e: FileNotFoundException) {
        Log.e(IO_EXCEPTION_TAG, e.message.toString())
        null
    } catch (e: IOException) {
        Log.e(IO_EXCEPTION_TAG, e.message.toString())
        null
    } catch (e: Exception) {
        Log.e(IO_EXCEPTION_TAG, e.message.toString())
        null
    }

    fun bitmapToFile(bitmap: Bitmap, file: File) = try {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(
            Bitmap.CompressFormat.JPEG,
            100,
            byteArrayOutputStream
        )

        val fileOutputStream = FileOutputStream(file)
        fileOutputStream.write(byteArrayOutputStream.toByteArray())
        fileOutputStream.close()
        byteArrayOutputStream.close()

        true
    } catch (e: FileNotFoundException) {
        Log.e(IO_EXCEPTION_TAG, e.message.toString())
        false
    } catch (e: IOException) {
        Log.e(IO_EXCEPTION_TAG, e.message.toString())
        false
    } catch (e: Exception) {
        Log.e(IO_EXCEPTION_TAG, e.message.toString())
        false
    }

    companion object {
        private const val IO_EXCEPTION_TAG = "IO_EXCEPTION"
        private const val IMAGE_DIR_NAME = "images"
        private const val filePrefix = "JPEG_CACHE"
        private const val fileSuffix = ".jpg"
    }
}