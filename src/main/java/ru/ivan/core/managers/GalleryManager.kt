package ru.ivan.core.managers

import android.app.Activity
import android.content.Intent
import android.net.Uri
import io.reactivex.Observable

class GalleryManager(
    private val permissionsManager: PermissionsManager,
    private val activityResultManager: ActivityResultManager,
) {
    /** запуск галереи */
    fun openGallery(requestCode: Int): Observable<GalleryStartingResult> =
        permissionsManager
            .check(PermissionsManager.WRITE_EXTERNAL_STORAGE)
            .flatMap { list ->
                if (list.isEmpty()) {
                    getPhotoFromGallery(requestCode)
                    return@flatMap Observable.just(GalleryStartingResult.GalleryStarted)
                }

                permissionsManager.request(list.toTypedArray())
                    .flatMap { result ->
                        if (result.items.isEmpty()) {
                            getPhotoFromGallery(requestCode)
                            return@flatMap Observable.just(GalleryStartingResult.GalleryStarted)
                        }

                        val showDialogList = result.items.filter {
                            it.shouldShowRequestPermissionRationale
                        }
                        if (showDialogList.isNotEmpty()) {
                            Observable.just(
                                GalleryStartingResult.CanNotShowPermissionDialog(showDialogList)
                            )
                        } else Observable.just(GalleryStartingResult.NotEnoughPermissions)
                    }
            }

    fun getOnPhotoCapturedObservable(requestCode: Int): Observable<PhotoCapturingResult> =
        activityResultManager
            .getOnActivityResultObservable(requestCode)
            .map {
                when (it.resultCode) {
                    Activity.RESULT_OK -> PhotoCapturingResult.Success(it)
                    else -> PhotoCapturingResult.Canceled
                }
            }

    private fun getPhotoFromGallery(requestCode: Int) {
        val intent = Intent(
            Intent.ACTION_PICK,
            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        ).apply {
            type = TYPE_IMAGE
        }
        activityResultManager.startActivityForResult(intent, null, requestCode)
    }

    sealed class GalleryStartingResult {
        object GalleryStarted : GalleryStartingResult()
        object Canceled : GalleryStartingResult()
        object NotEnoughPermissions : GalleryStartingResult()
        data class CanNotShowPermissionDialog(val list: List<PermissionsManager.Permission>) :
            GalleryStartingResult()
    }

    sealed class PhotoCapturingResult {
        data class Success(val result: ActivityResult) : PhotoCapturingResult()
        object Canceled : PhotoCapturingResult()
    }

    companion object {
        private const val TYPE_IMAGE = "image/*"

        fun getPhotosFromData(data: Intent?): List<Uri> {
            if (data === null) return emptyList()

            val clipData = data.clipData
            if (clipData != null) {
                return IntRange(0, clipData.itemCount - 1).map {
                    clipData.getItemAt(it).uri
                }
            }

            val data2 = data.data
            if (data2 != null) {
                return listOf(Uri.parse(data2.toString()))
            }
            return emptyList()
        }
    }
}