package ru.ivan.core.managers

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.provider.CalendarContract
import io.reactivex.Observable
import org.joda.time.DateTime


class CalendarManager(
    private val context: Context,
    private val permissionsManager: PermissionsManager
) {

    @SuppressLint("MissingPermission")
    private fun addEventsToCalender(events: List<CalendarEvent>): Int {
        val result = events.map {
            ContentValues().apply {
                put(CalendarContract.Events.CALENDAR_ID, 1) // id, We need to choose from
                put(CalendarContract.Events.TITLE, it.title)
                put(CalendarContract.Events.DESCRIPTION, it.description)

                put(CalendarContract.Events.DTSTART, it.startDate.millis)
                put(CalendarContract.Events.DTEND, it.startDate.millis)
                put(CalendarContract.Events.EVENT_TIMEZONE, "Russia/Moscow")
            }
        }.toTypedArray()

        val eventUri = context.contentResolver
            .bulkInsert(CalendarContract.Events.CONTENT_URI, result)

        return eventUri
    }

    fun writeEventsToCalendar(events: List<CalendarEvent>): Observable<CalendarWritingResult> =
        permissionsManager
            .check(PermissionsManager.READ_CALENDAR, PermissionsManager.WRITE_CALENDAR)
            .flatMap { list ->
                if (list.isEmpty()) {
                    return@flatMap Observable.fromCallable {
                        CalendarWritingResult.EventCreated(addEventsToCalender(events))
                    }
                }

                return@flatMap permissionsManager.request(list.toTypedArray())
                    .flatMap { result ->
                        if (result.items.isEmpty()) {
                            return@flatMap Observable.fromCallable {
                                CalendarWritingResult.EventCreated(addEventsToCalender(events))
                            }
                        }

                        val showDialogList = result.items.filter {
                            it.shouldShowRequestPermissionRationale
                        }
                        if (showDialogList.isNotEmpty()) {
                            Observable.just(
                                CalendarWritingResult.PermissionRejected(showDialogList)
                            )
                        } else Observable.just(CalendarWritingResult.PermissionDisabled)
                    }
            }

    data class CalendarEvent(val title: String, val description: String, val startDate: DateTime)

    sealed class CalendarWritingResult {
        data class EventCreated(val id: Int) : CalendarWritingResult()
        object PermissionDisabled : CalendarWritingResult()
        data class PermissionRejected(val list: List<PermissionsManager.Permission>) :
            CalendarWritingResult()
    }
}