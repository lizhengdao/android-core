package ru.ivan.core.util.validation

import java.util.regex.Pattern

/**
 * Created by i.sokolovskiy on 18.09.19.
 */
class NameValidator(errorMsg: String) : PatternValidator(
    Pattern.compile("^(?=[ёЁA-Za-zА-Яа-я ]{2,64}$)\\S+( \\S+)?$"),
    errorMsg
)