package ru.ivan.core.util.validation

import android.util.Log

class RangeValidator(
    private val minValue: Int,
    private val maxValue: Int,
    private val errorMsg: String
) : ITextValidator {
    override fun validate(text: CharSequence): ValidationResult {
        val newValue = try {
            text.toString().toInt()
        } catch (e: NumberFormatException) {
            Log.e("NumberValidator", e.message.toString())
            0
        }
        return if (newValue in minValue..maxValue) ValidationResult.Ok
        else ValidationResult.Error(errorMsg)
    }
}