package ru.ivan.core.util.validation

/**
 * Created by i.sokolovskiy on 19.09.19.
 */
open class NotNullValidator(
    private val errorMsg: String
) : IObjectValidator {
    override fun <T> validate(obj: T): ValidationResult {
        return if (obj == null) ValidationResult.Error(errorMsg) else ValidationResult.Ok
    }
}