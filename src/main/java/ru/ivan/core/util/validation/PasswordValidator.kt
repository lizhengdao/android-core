package ru.ivan.core.util.validation

import java.util.regex.Pattern

/**
 * Created by i.sokolovskiy on 18.09.19.
 */
class PasswordValidator(errorMsg: String) : PatternValidator(
    Pattern.compile("^[a-zA-Z0-9~!@#$%^&*()_+`\\-={}\\[\\]:;<>./\\\\]{6,32}$"),
    errorMsg
)