package ru.ivan.core.util.validation

/**
 * Created by i.sokolovskiy on 19.09.19.
 */
interface ITextValidator {
    fun validate(text: CharSequence): ValidationResult
}