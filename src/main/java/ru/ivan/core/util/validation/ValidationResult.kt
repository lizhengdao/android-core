package ru.ivan.core.util.validation

/**
 * Created by i.sokolovskiy on 19.09.19.
 */
sealed class ValidationResult {
    object Ok : ValidationResult()
    data class Error(val errorMsg: String) : ValidationResult()

    inline fun <V> processResult(
        successHandler: (Ok) -> V,
        errorHandler: (Error) -> V
    ): V =
        when (this) {
            is Ok -> successHandler(this)
            is Error -> errorHandler(this)
        }

    fun isOk(): Boolean =
        this is Ok

    fun isError(): Boolean =
        this is Error
}