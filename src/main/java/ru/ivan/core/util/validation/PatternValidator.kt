package ru.ivan.core.util.validation

import java.util.regex.Pattern

/**
 * Created by i.sokolovskiy on 18.09.19.
 */
open class PatternValidator(private val pattern: Pattern, private val errorMsg: String) :
    ITextValidator {
    override fun validate(text: CharSequence): ValidationResult =
        if (pattern.matcher(text.trim()).matches()) ValidationResult.Ok else ValidationResult.Error(
            errorMsg
        )
}
