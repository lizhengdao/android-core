package ru.ivan.core.util.validation

/**
 * Created by i.sokolovskiy on 19.09.19.
 */
open class LengthValidator(
    private val minSymbols: Int,
    private val maxSymbols: Int,
    private val errorMsg: String
) : ITextValidator {
    override fun validate(text: CharSequence): ValidationResult {
        return if (text.length < minSymbols || text.length > maxSymbols) ValidationResult.Error(
            errorMsg
        ) else ValidationResult.Ok
    }
}