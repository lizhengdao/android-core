package ru.ivan.core.util.validation

/**
 * Created by i.sokolovskiy on 19.09.19.
 */
open class NotEmptyValidator(
    private val errorMsg: String
) : ITextValidator {
    override fun validate(text: CharSequence): ValidationResult {
        return if (text.isEmpty()) ValidationResult.Error(errorMsg) else ValidationResult.Ok
    }
}