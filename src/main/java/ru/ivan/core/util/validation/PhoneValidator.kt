package ru.ivan.core.util.validation

import java.util.regex.Pattern

/**
 * Created by i.sokolovskiy on 18.09.19.
 */
class PhoneValidator(errorMsg: String) : PatternValidator(
    Pattern.compile("^\\+79\\d{9}$"),
    errorMsg
)