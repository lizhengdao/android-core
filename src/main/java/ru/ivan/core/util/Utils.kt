package ru.ivan.core.util

import ru.ivan.core.util.validation.IObjectValidator
import ru.ivan.core.util.validation.ITextValidator
import ru.ivan.core.util.validation.ValidationResult

object Utils {
    fun generateRequestCode() = (0..65535).random()

    fun validateField(
        text: String,
        allowEmpty: Boolean,
        vararg validators: ITextValidator
    ): ValidationResult {
        if (allowEmpty && text.isBlank()) return ValidationResult.Ok

        for (validator in validators) {
            val validationResult = validator.validate(text)
            if (validationResult is ValidationResult.Error) return validationResult
        }
        return ValidationResult.Ok
    }

    fun <T : Any?> validateField(
        obj: T,
        vararg validators: IObjectValidator
    ): ValidationResult {
        for (validator in validators) {
            val validationResult = validator.validate(obj)
            if (validationResult is ValidationResult.Error) return validationResult
        }
        return ValidationResult.Ok
    }
}