package ru.ivan.core.util

import android.app.DatePickerDialog
import android.content.Context
import java.util.*

object DialogUtils {
    /**
     * @param currentDate текущая дата
     * @param maxDate максимальная дата
     * @param onDateSet listener для получения выбранной даты
     */
    fun showDatePicker(
        context: Context,
        currentDate: Date?,
        maxDate: Date?,
        onDateSet: (calendar: Calendar) -> Unit
    ): DatePickerDialog {
        // init
        val calendar = Calendar.getInstance()

        currentDate?.let { calendar.time = it }

        val dialog = DatePickerDialog(
            context,
            DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                val selectedDate = Calendar.getInstance()

                selectedDate.set(Calendar.YEAR, year)
                selectedDate.set(Calendar.MONTH, month)
                selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                onDateSet(selectedDate)
            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )

        maxDate?.let { dialog.datePicker.maxDate = it.time }

        return dialog
    }
}