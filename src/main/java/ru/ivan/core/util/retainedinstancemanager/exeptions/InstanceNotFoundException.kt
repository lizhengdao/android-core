package ru.ivan.core.util.retainedinstancemanager.exeptions

class InstanceNotFoundException : Throwable("Instance was not found")