package ru.ivan.core.util.retainedinstancemanager

interface IHasRetainedInstance<T> {
    fun createRetainedInstance(): T
}