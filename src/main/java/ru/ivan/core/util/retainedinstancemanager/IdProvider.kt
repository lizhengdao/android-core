package ru.ivan.core.util.retainedinstancemanager

interface IdProvider {
    fun getUuid(): String
}