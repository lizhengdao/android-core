package ru.ivan.core.di

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import ru.ivan.core.managers.*

fun managersCoreDiModule() = Kodein.Module("managersCoreDiModule") {
    bind<PermissionsManager>() with singleton { PermissionsManager(context = instance()) }
    bind<ActivityResultManager>() with singleton { ActivityResultManager() }
    bind<AppFileManager>() with singleton { AppFileManager(context = instance()) }
    bind<CameraManager>() with singleton {
        CameraManager(
            context = instance(),
            activityResultManager = instance(),
            permissionsManager = instance(),
            appFileManager = instance()
        )
    }
    bind<GalleryManager>() with singleton {
        GalleryManager(
            permissionsManager = instance(),
            activityResultManager = instance()
        )
    }

    bind<LocationManager>() with singleton {
        LocationManager(
            instance(),
            instance(),
            instance()
        )
    }

    bind<CalendarManager>() with singleton {
        CalendarManager(
            instance(),
            instance()
        )
    }
}