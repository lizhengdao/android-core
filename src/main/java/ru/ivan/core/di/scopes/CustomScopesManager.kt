package ru.ivan.core.di.scopes

import android.app.Application
import ru.ivan.core.util.retainedinstancemanager.helpers.ActivityLifecycleHelper

/**
 * Created by i.sokolovskiy on 20.12.19.
 */
object CustomScopesManager {
    fun init(app: Application) = app.registerActivityLifecycleCallbacks(
        ActivityLifecycleHelper(
            onActivityDestroyed = { activity -> CustomActivityScope.clearScope(activity) },
            onFragmentDestroyed = { fragment -> CustomFragmentScope.clearScope(fragment) }
        )
    )
}