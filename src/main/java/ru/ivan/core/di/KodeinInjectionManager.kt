package ru.ivan.core.di

import android.app.Application
import org.kodein.di.Kodein
import ru.ivan.core.util.retainedinstancemanager.IHasRetainedInstance
import ru.ivan.core.util.retainedinstancemanager.RetainedInstanceManager


class KodeinInjectionManager {
    private val injectionManager =
        RetainedInstanceManager<Kodein>()

    companion object {
        @JvmStatic
        val instance = KodeinInjectionManager()
    }

    fun init(app: Application) = injectionManager.init(app)

    fun bindKodein(owner: IHasRetainedInstance<Kodein>) = injectionManager.bindKodein(owner)
}