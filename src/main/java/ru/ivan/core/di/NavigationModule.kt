package ru.ivan.core.di

import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.factory
import org.kodein.di.generic.scoped
import org.kodein.di.generic.singleton
import ru.ivan.core.di.scopes.CustomActivityScope
import ru.ivan.core.di.scopes.CustomFragmentScope
import ru.ivan.core.unclassifiedcommonmodels.CustomNavigator
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.Coordinator
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.CoordinatorHolder
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.StubCoordinator
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router

/**
 * Created by i.sokolovskiy on 19.03.19.
 */
fun navigationDiModule() = Kodein.Module("navigationDiModule") {
    bind<Cicerone<Router>>() with scoped(CustomFragmentScope).singleton { Cicerone.create() }

    bind<Cicerone<Router>>() with scoped(CustomActivityScope).singleton { Cicerone.create() }

    bind<CoordinatorHolder>() with scoped(CustomFragmentScope).singleton { CoordinatorHolder() }

    bind<CoordinatorHolder>() with scoped(CustomActivityScope).singleton { CoordinatorHolder() }

    bind<Coordinator>() with singleton { StubCoordinator() }

    bind<CustomNavigator>() with factory { param: CustomNavigatorParam ->
        CustomNavigator(
            param.activity, param.fragmentManager, param.containerId, param.onExit
        )
    }
}

data class CustomNavigatorParam(
    val activity: FragmentActivity, val fragmentManager: FragmentManager, val containerId: Int,
    val onExit: () -> Unit
)