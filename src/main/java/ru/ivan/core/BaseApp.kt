package ru.ivan.core

import androidx.multidex.MultiDexApplication
import com.facebook.stetho.Stetho
import net.danlew.android.joda.JodaTimeAndroid
import org.kodein.di.KodeinAware
import ru.ivan.core.di.KodeinInjectionManager
import ru.ivan.core.di.scopes.CustomScopesManager

abstract class BaseApp : MultiDexApplication(), KodeinAware {

    override fun onCreate() {
        super.onCreate()

        appInstance = this

        KodeinInjectionManager.instance.init(this)
        CustomScopesManager.init(this)

        Stetho.initializeWithDefaults(this)

        JodaTimeAndroid.init(this)
    }

    companion object {
        lateinit var appInstance: BaseApp
            private set
    }
}