package ru.ivan.core

import ru.terrakok.cicerone.android.support.SupportAppScreen

object BaseScreens {
    data class WebBrowser(val site: String) : SupportAppScreen()
    data class Email(val mail: String) : SupportAppScreen()
    data class Phone(val phone: String) : SupportAppScreen()
}